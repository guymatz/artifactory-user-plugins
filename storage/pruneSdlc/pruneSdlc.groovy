/**
 * Execute a pruning of files in the prod repo every time a new version is added,
 * by keeping only the latest 3 versions of the uploaded app
 * Then find the last version of the uploaded app and remove up till that point from
 * all of the other sidecar repos.  I'll explain this better later , , ,
 *
 * @author Guy Matz
 * @since 5/11/16
 */


storage {
    def config = new ConfigSlurper().parse(new File("${System.properties.'artifactory.home'}/etc/plugins/pruneSdlc.properties").toURL())

    afterCopy { item, targetRepoPath, properties ->
        log.debug("afterCopy: item = ${item}, targetRepoPath = ${targetRepoPath}")
        try {
            log.debug("afterCopy: I see ${targetRepoPath.getRepoKey()}")
            if (targetRepoPath.getRepoKey().toString() != config.prod_repo) {
                log.debug("afterCopy: NOT PROD I see ${targetRepoPath}")
                return
            }
            leaveNartifacts(targetRepoPath, config.retention)
            log.debug("afterCopy: ${targetRepoPath.getRepoKey()} ${targetRepoPath} has ${repositories.getArtifactsCount(targetRepoPath.getParent())} artifacts")
            log.debug("afterCopy: **************** config.retention = ${config.retention}")
            if (repositories.getArtifactsCount(targetRepoPath.getParent()) >= config.retention) {
                oldest_rpm = getOldest(targetRepoPath, config.prod_repo)
                log.debug("afterCopy: oldest = " + oldest_rpm)
                remove_from_non_prod_before(oldest_rpm, config.non_prod_repos.split(','))
                log.debug("SEARCHES:" + searches.artifactsByName(getApp(item), item.repoPath.getRepoKey()).findAll({ a -> getVersion(a) > getVersion(artifact)}))
                log.debug("afterCopy: SEARCHES:" + searches.artifactsByName("${getApp(item)}*", item.repoPath.getRepoKey()))
            }
        }
        catch (Exception e) {
            log.warn("afterCopy: ERROR: " + e)
        }
    }

}

def remove_from_non_prod_before(artifact, repos) {
    log.debug("remove_from_non_prod_before: deleting before ${artifact} from ${repos}")
    for (repo in repos) {
        def search_str = "${getApp(artifact)}*"
        log.debug("remove_from_non_prod_before: Looking for ${search_str} in ${repo}")
        to_delete = searches.artifactsByName(search_str, repo).findAll({ a -> getVersion(a) <= getVersion(artifact)})
        log.debug("remove_from_non_prod_before: I FOUND: " + to_delete)
        log.debug("I FOUND: " + searches.artifactsByName(search_str, repo))
        to_delete.each { r ->
            try {
                repositories.delete(r)
                log.warn("remove_from_non_prod_before: DELETED = ${r.toString()}")
            }
            catch (Exception e) {
                log.warn("remove_from_non_prod_before: ERROR: deleting ${r.toString()} : " + e)
            }
        }
    }
}

def getApp(artifact) {
    //  matching version this way *seems* more reliable that split'ing
    // Since we have sidecar-web-###-1.x86  and sidecar-madewell-web-###-1.x86
    log.debug("getApp: artifact = ${artifact}")
    def regex = /(.*?)[0-9].*/     // non-greedy match, should change to -[0-9]+-
    try {
        app = (artifact.getName() =~ regex)[0][1]
        log.debug("getApp: app = ${app}")
        return app
    }
    catch (Exception e) {
        log.warn("getApp: Could not get app from ${artifact}")
        return 'UNKOWN'
    }
}

def getOldest(artifact, repo) {
    /* This method does not actually get the correct oldest file in the repo
       To get the correct one I would have to use a workerThread as exemplified in this snippet
       Actually, I think I need to use the workerThread in leaveNartifacts

       import org.artifactory.repo.RepoPathFactory
       import org.artifactory.schedule.CachedThreadPoolTaskExecutor

       // The thread pool used internally by Artifactory. This pool executes tasks in
       // the same environment that user plugins are run in, which allows them access
       // to all the global Artifactory variables (repositories, builds, log, etc), the
       // database, and so on. These things wouldn't be allowed in a standard pool.
       workerThread = ctx.beanForType(CachedThreadPoolTaskExecutor)

       storage {
           // Use afterCreate to modify uploads. If you want to reject uploads, use
           // beforeCreate and throw an org.artifactory.exception.CancelException.
           afterCreate { item ->
               // If an artifact is uploaded to a location that doesn't exist,
               // afterCreate is run once for each new directory, and then once for the
               // artifact itself. This is usually not what you want.
               if (item.isFolder()) return
               workerThread.submit {
                   // Move the file that was just uploaded, or whatever needs to be
                   // done. This does not automatically delete folders created during
                   // the artifact upload, so if you want that done you'll have to do
                   // it manually here.
                   repositories.move(item.repoPath,
                           RepoPathFactory.create(item.repoKey, 'other/dir/file.txt'))
               }
           }
       }
    */
    def search_str = "${getApp(artifact)}*"

    try {
        sorted_search = searches.artifactsByName(search_str, repo).sort({ a, b -> getVersion(a) <=> getVersion(b) })
        oldest = sorted_search[0]
    }
    catch (Exception e) {
        log.debug("getOldest: Could not sort: ${e}")
        return 'UNKOWN'
    }
    log.warn("getOldest: Oldest found in sorted search: ${oldest}")
    return oldest
}

def getVersion(artifact) {
    //  matching version this way *seems* more reliable that split'ing
    // Since we have sidecar-web-###-1.x86  and sidecar-madewell-web-###-1.x86
    // log.warn("DO I GET HERE? getVersion")
    log.debug("getVersion: artifact = ${artifact}")
    def regex = /.*?([0-9]+).*/     // non-greedy match, should change to -[0-9]+-
    try {
        return (artifact.getName() =~ regex)[0][1].toInteger()
    }
    catch (Exception e) {
        return 'UNKOWN'
    }
}

def leaveNartifacts(artifact, num_to_leave) {
    // Deletes artifacts in a repo based on a retention policy
    log.debug("leaveNartifacts artifact = ${artifact}")
    def regex = /(.*?)[0-9].*/  // DEV non-greedy match, should change to -[0-9]+-
    // def regex = /.*?([0-9]+).*/     // PROD non-greedy match, should change to -[0-9]+-
    def app = (artifact.getName() =~ regex)[0][1]
    def repo = artifact.getRepoKey()
    log.debug("leaveNartifacts: app = ${app}")
    def res = searches.artifactsByName("${app}*", repo)
    log.debug("leaveNartifacts: results = ${res.sort({ a,b -> getVersion(a) <=> getVersion(b)})}")
    if (res.size() < num_to_leave) {
        log.debug("leaveNartifacts: Only ${res.size()} elements of ${app} in ${repo}")
        return
    }
    to_delete = res.sort({ a,b -> getVersion(a) <=> getVersion(b)})[0..-(num_to_leave)]
    log.debug("leaveNartifacts: to_delete = ${to_delete}")
    to_delete.each { r ->
        log.warn("leaveNartifacts: deleting = ${r.toString()}")
        repositories.delete(r)
    }
}
