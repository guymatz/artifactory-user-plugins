import org.artifactory.repo.RepoPathFactory
import org.artifactory.schedule.CachedThreadPoolTaskExecutor

/**
 * Execute a check to determine whether there are new files in the repo
 * that need requires the repo to be re-indexed
 * sample call:
 * curl -s -X POST -uadmin:xxxxxx http://localhost:8088/artifactory/api/plugins/execute/checkReindex?params=repo=sidecar-ci
 * sample output:
 * {"reindexRequired":"false"}
 * @author Guy Matz
 * @since 6/7/16
 */

/* From JFrog support:
 The thread pool used internally by Artifactory. This pool executes tasks in
 the same environment that user plugins are run in, which allows them access
 to all the global Artifactory variables (repositories, builds, log, etc), the
 database, and so on. These things wouldn't be allowed in a standard pool.
*/

workerThread = ctx.beanForType(CachedThreadPoolTaskExecutor)

executions {
    checkReindex(httpMethod: 'GET', users:['anonymous']) { params ->
        String repo = params?.get('repo')?.get(0) as String
        if (!repo) {
            status = 400
            message = "Need a path parameter to calculate YUM metadata"
            return
        }
        def repoPath = RepoPathFactory.create(repo)
        if (!repositories.exists(repoPath)) {
            status = 404
            message = "Repo $repoPath.id does not exist"
            return
        }
        repoConf = repositories.getRepositoryConfiguration(repoPath.repoKey)
        if (repoConf.packageType != 'yum') {
            status = 400
            message = "Given repository $repoPath.repoKey not a YUM repository"
            return
        }

        def search_str = "*.rpm"
        log.debug("Looking for ${search_str} in ${repo}")
        // Check to see if any artifacts in the repo have not been tagged with the
        // property 'rpm.metadata.name'.  This indicates a reindexing is required
        def needsReindex = true
        try {
            needsReindex = searches.artifactsByName(search_str, repo).any({ artifact ->
                !repositories.hasProperty(artifact, 'rpm.metadata.name')
            })
        }
        catch (Exception e) {
            status = 400
            message = "Something went wrong with the property check" + e
            return
        }
        status = 200
        message = '{"reindexRequired":"' + needsReindex.toString() + '"}'
        return
    }
}

storage {
    // This is required to remove metadata from RPMs copied between yum repos.  If
    // The metadata is not removed, then the check above won't work so well
    afterCopy { item, targetRepoPath, properties ->
        props = repositories.getProperties(targetRepoPath)
        if (item.isFolder()) return
        log.warn("checkReindex.afterCopy: item = ${item} to targetRepoPath = ${targetRepoPath}")
        workerThread.submit {
            props.keySet().each { prop ->
            // log.warn("aCopy prop methods = ${prop.metaClass.methods.name.sort()}")
                if (prop.startsWith('rpm.metadata')) {
                    try {
                        log.debug("checkReindex.afterCopy: Attempting to delete ${prop} from ${targetRepoPath}")
                        repositories.deleteProperty(targetRepoPath, prop)
                    }
                    catch (Exception e) {
                        log.warn("Oops! " + e)
                    }
                }
            }
        }

    }
}
